﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //DowloadfromBlob();

            
              var d = Directory.CreateDirectory("d:\\temp\\unzipfromcode2");
            Directory.Delete(d.FullName, true);
            d = Directory.CreateDirectory("d:\\temp\\unzipfromcode2");
            ZipFile.ExtractToDirectory("d:\\temp\\unzipfromcode.zip", d.FullName);

            

           // BCPfromFile("D:\\CaseProjects\\AzureFunctionsZipBCP\\cars.csv");
        }
        
        static void DowloadfromBlob()
        {
            var storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=;AccountKey===");
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("bcp");
            var blob = container.GetBlockBlobReference("Cars.zip");
            blob.DownloadToFile("d:\\temp\\cars.zip", FileMode.CreateNew);

         
        }

        static void BCPfromFile(string filename)
        {

            DataTable sourceData = new DataTable();

            sourceData.Columns.Add("CarMake");
            sourceData.Columns.Add("CarModel");
            sourceData.Columns.Add("CarYear");

           

            using (var rd = new StreamReader(filename))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    sourceData.Rows.Add(splits[0], splits[1], splits[2]);
                }
            }

            SqlBulkCopy bcp = new SqlBulkCopy("Server=tcp:.database.windows.net,1433;Initial Catalog=;Persist Security Info=False;User ID=;Password=;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            bcp.DestinationTableName = "bcp";
            bcp.WriteToServer(sourceData);



        }

    }
}


